package com.daly.calculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        mEditText1.setOnClickListener({
            appendText("1", true)
        })
        mEditText2.setOnClickListener({
            appendText("2", true)
        })
        mEditText3.setOnClickListener({
            appendText("3", true)
        })
        mEditText4.setOnClickListener({
            appendText("4", true)
        })
        mEditText5.setOnClickListener({
            appendText("5", true)
        })
        mEditText6.setOnClickListener({
            appendText("6", true)
        })
        mEditText7.setOnClickListener({
            appendText("7", true)
        })
        mEditText8.setOnClickListener({
            appendText("8", true)
        })
        mEditText9.setOnClickListener({
            appendText("9", true)
        })
        mEditTextPlus.setOnClickListener({
            appendText("+", false)
        })
        mEditTextMinus.setOnClickListener({
            appendText("-", false)
        })
        mEditTextMultiply.setOnClickListener({
            appendText("*", false)
        })
        mEditTextDivide.setOnClickListener({
            appendText("/", false)
        })
        mEditTextDot.setOnClickListener({
            appendText(".", false)
        })
        mEditTextZero.setOnClickListener({
            appendText("0", true)
        })
        mEditTextClean.setOnClickListener({


            var currentValue = mTextViewExpression.text.toString()

            if (currentValue.isNotEmpty()) {

                mTextViewExpression.text =
                        mTextViewExpression.text.toString().substring(0, mTextViewExpression.text.toString().length - 1)
                mTextViewValue.text = ""
            }


        })
        mEditTextOpen.setOnClickListener({
            appendText("(", true)
        })
        mEditTextClose.setOnClickListener({
            appendText(")", true)
        })
        mEditTextCE.setOnClickListener({
            appendText("", true)
        })

        mEditTextEqual.setOnClickListener({


            val expression = ExpressionBuilder(mTextViewExpression.text.toString()).build()
            val result = expression.evaluate()
            val longResult = result.toLong()
            if (result == longResult.toDouble()) {
                mTextViewValue.text = longResult.toString()
            } else
                mTextViewValue.text = result.toString()
        })


    }

    fun appendText(value: String, canClean: Boolean) {

        if (mTextViewValue.text.isNotEmpty()) {
            mTextViewExpression.text = ""
        }

        if (canClean) {
            mTextViewValue.text = ""
            mTextViewExpression.append(value);
        } else {
            mTextViewExpression.append(mTextViewValue.text)
            mTextViewExpression.append(value)
            mTextViewValue.text = ""
        }


    }
}
