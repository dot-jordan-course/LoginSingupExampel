package com.daly.loginexample

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mButtonLogin.setOnClickListener({
            loginClickAction()
        })
    }

    fun loginClickAction() {
        mEditTextEmail.error = null
        mEditTextPassword.error = null
        var emailText = mEditTextEmail.text.toString()
        var passwordText = mEditTextPassword.text.toString()
        var validation: Boolean = true;

        if (emailText.isEmpty()) {
            mEditTextEmail.error = "Please check the email"
            validation = false
        }

        if (passwordText.isEmpty()) {
            mEditTextPassword.error = "Please check the password"
            validation = false
        }

        if (validation) {
            var intent = Intent(this, ResultActivity::class.java)
            intent.putExtra("email", emailText)
//            intent.putExtra("password", passwordText)
            startActivity(intent)
        }
    }
}
