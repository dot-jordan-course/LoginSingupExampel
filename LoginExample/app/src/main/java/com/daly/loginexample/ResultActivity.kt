package com.daly.loginexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_result.*

class ResultActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        getExtra()
    }

    fun getExtra() {
        var emailText = intent.getStringExtra("email")
        var passwordText = intent.getStringExtra("password")
        mTextViewEmail.text = emailText
        mTextViewPassword.text = passwordText
    }
}
