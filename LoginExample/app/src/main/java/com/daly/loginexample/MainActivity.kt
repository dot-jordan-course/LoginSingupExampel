package com.daly.loginexample

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        mButtonLogin.setOnClickListener({

            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)

        })

        mButtonSignup.setOnClickListener({

            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)

        })
    }
}
