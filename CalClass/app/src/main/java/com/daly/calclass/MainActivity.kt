package com.daly.calclass

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.Expression
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        mEditText0.setOnClickListener({

            handleClick("0")
        })
        mEditText1.setOnClickListener({

            handleClick("1")
        })
        mEditText2.setOnClickListener({

            handleClick("2")
        })
        mEditText3.setOnClickListener({

            handleClick("3")
        })
        mEditText4.setOnClickListener({

            handleClick("4")
        })
        mEditText5.setOnClickListener({

            handleClick("5")
        })
        mEditText6.setOnClickListener({

            handleClick("6")
        })

        mEditText7.setOnClickListener({

            handleClick("7")
        })
        mEditText8.setOnClickListener({

            handleClick("8")
        })
        mEditText9.setOnClickListener({

            handleClick("9")
        })

        mEditTextOpen.setOnClickListener({

            handleClick("(")
        })
        mEditTextClose.setOnClickListener({

            handleClick(")")
        })
        mEditTextDivide.setOnClickListener({

            handleClick("/")
        })
        mEditTextMinus.setOnClickListener({

            handleClick("-")
        })
        mEditTextMultiply.setOnClickListener({

            handleClick("*")
        })
        mEditTextDot.setOnClickListener({

            handleClick(".")
        })

        mEditTextCE.setOnClickListener({

            mTextViewExpression.text = ""
            mTextViewValue.text = ""

        })

        mEditTextEqual.setOnClickListener({


            try {
                val expression = ExpressionBuilder(mTextViewExpression.text.toString()).build()
                Log.d("MHMT", expression.toString())
                val result = expression.evaluate()
                val longResult = result.toLong()
                mTextViewValue.text = longResult.toString()

            } catch (ex: Exception) {

            }


        })
        mEditTextClean.setOnClickListener({

            var text = mTextViewExpression.text.toString()
            mTextViewExpression.text = text.substring(0, text.length - 1)


        })


    }

    fun handleClick(value: String) {


        mTextViewExpression.append(value)


    }
}
